package cn.zjh.stock.service;

import cn.zjh.stock.pojo.domain.*;
import cn.zjh.stock.pojo.vo.resp.PageResult;
import cn.zjh.stock.pojo.vo.resp.R;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

/**
 * @ClassName StockService
 * @Description //股票相关服务接口
 * @Author Rum
 * @Date 2024/7/16 17:18
 * @Version
 **/
public interface StockService {
    R<List<InnerMarketDomain>> getInnerMarket();

    R<PageResult<StockUpDownDomain>> getStockInfoByPage(Integer page, Integer pageSize);

    R<List<StockUpDownDomain>> getStockInfoIncrease();

    R<Map<String, List>> getStockUpDownCount();

    void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response);

    R<Map<String, List>> getStockTradeAmount();

    R<Map> getIncreaseRangeInfo();

    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);

    R<List<Stock4EvrDayDomain>> getStockScreenDkLine(String stockCode);

    R<List<Stock4OuterDomain>> getOuterMarketInfo();

    R<List<Stock4SearchDomain>> getStockSearchStr(String searchStr);

    R<Stock4DescribeDomain> getStockDescribe(String stockCode);

}
