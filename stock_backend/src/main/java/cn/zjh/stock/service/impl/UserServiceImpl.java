package cn.zjh.stock.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.zjh.stock.constant.StockConstant;
import cn.zjh.stock.mapper.SysUserMapper;
import cn.zjh.stock.pojo.entity.SysUser;
import cn.zjh.stock.pojo.vo.req.LoginReqVo;
import cn.zjh.stock.pojo.vo.resp.LoginRespVo;
import cn.zjh.stock.pojo.vo.resp.R;
import cn.zjh.stock.pojo.vo.resp.ResponseCode;
import cn.zjh.stock.service.UserService;
import cn.zjh.stock.utils.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName UserServiceImpl
 * @Description //用户服务实现类
 * @Author Rum
 * @Date 2024/7/11 15:59
 * @Version
 **/
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public SysUser findUserInfoByUsername(String userName) {
        return sysUserMapper.selectUserInfoByUsername(userName);
    }

    @Override
    public R<LoginRespVo> login(LoginReqVo loginReqVo) {
        if (loginReqVo == null || StringUtils.isBlank(loginReqVo.getUsername()) || StringUtils.isBlank(loginReqVo.getPassword())) {
            return R.error(ResponseCode.DATA_ERROR);
        }
        if (StringUtils.isBlank(loginReqVo.getCode()) || StringUtils.isBlank(loginReqVo.getSessionId())) {
            return R.error(ResponseCode.CHECK_CODE_NOT_EMPTY);
        }
        String redisCode = (String) redisTemplate.opsForValue().get(StockConstant.CHECK_PREFIX + loginReqVo.getSessionId());
        if (StringUtils.isBlank(redisCode)) {
            return R.error(ResponseCode.CHECK_CODE_TIMEOUT);
        }
        if (!redisCode.equals(loginReqVo.getCode())) {
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }

        SysUser dbUser = sysUserMapper.selectUserInfoByUsername(loginReqVo.getUsername());

        if (dbUser == null) {
            return R.error(ResponseCode.ACCOUNT_NOT_EXISTS);
        }
        if (!passwordEncoder.matches(loginReqVo.getPassword(), dbUser.getPassword())) {
            return R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }
        LoginRespVo loginRespVo = new LoginRespVo();
        //属性名称与类型必须相同，否则属性值无法copy
        BeanUtils.copyProperties(dbUser, loginRespVo);
        return R.ok(loginRespVo);
    }

    /**
     * @return cn.zjh.stock.domain.vo.resp.R<java.util.Map>
     * @Author Rum
     * @Description //生成图片验证码
     * @Date 17:28 2024/7/13
     * @Param []
     **/
    @Override
    public R<Map> getCaptchaCode() {
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(250, 40, 4, 5);
        captcha.setBackground(Color.LIGHT_GRAY);
        String checkCode = captcha.getCode();
        String imageData = captcha.getImageBase64();
        String sessionId = String.valueOf(idWorker.nextId());
        log.info("当前生成的图片校验码：{},会话id；{}", checkCode, sessionId);
        redisTemplate.opsForValue().set(StockConstant.CHECK_PREFIX + sessionId, checkCode, 5, TimeUnit.MINUTES);
        Map<String, String> data = new HashMap<>();
        data.put("imageData", imageData);
        data.put("sessionId", sessionId);
        return R.ok(data);
    }
}
