package cn.zjh.stock.service.impl;

import cn.zjh.stock.mapper.StockBusinessMapper;
import cn.zjh.stock.mapper.StockMarketIndexInfoMapper;
import cn.zjh.stock.mapper.StockOuterMarketIndexInfoMapper;
import cn.zjh.stock.mapper.StockRtInfoMapper;
import cn.zjh.stock.pojo.domain.*;
import cn.zjh.stock.pojo.vo.StockInfoConfig;
import cn.zjh.stock.pojo.vo.resp.PageResult;
import cn.zjh.stock.pojo.vo.resp.R;
import cn.zjh.stock.pojo.vo.resp.ResponseCode;
import cn.zjh.stock.service.StockService;
import cn.zjh.stock.utils.DateTimeUtil;
import com.alibaba.excel.EasyExcel;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName StockServiceImpl
 * @Description 股票服务实现类
 * @Author Rum
 * @Date 2024/7/16 17:20
 * @Version
 **/
@Service
@Slf4j
public class StockServiceImpl implements StockService {
    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @Autowired
    private StockInfoConfig stockInfoConfig;
    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;
    @Autowired
    private StockOuterMarketIndexInfoMapper stockOuterMarketIndexInfoMapper;
    @Autowired
    private StockBusinessMapper stockBusinessMapper;

    /**
     * @Author Rum
     * @Description 注入本地缓存bean
     * @Date 16:55 2024/7/21
     * @Param
     * @return
     **/
    @Autowired
    private Cache<String,Object> caffeineCache;

    @Override
    public R<List<InnerMarketDomain>> getInnerMarket() {
        //默认从本地缓存加载数据，如果不存在则从数据库加载并同步到本地缓存
        //在开盘周期内，本地缓存默认有效期1分钟
        R<List<InnerMarketDomain>> result = (R<List<InnerMarketDomain>>) caffeineCache.get("innerMarketKey", key -> {
            //获取当前日期并转化日期为数据库支持的格式
            Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
            //mock data 等后续完成股票采集job工程，再将代码修改
            curDate = DateTime.parse("2022-01-02 09:32:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
            //获取股票代码集合
            List<String> mCode = stockInfoConfig.getInner();
            List<InnerMarketDomain> data = stockMarketIndexInfoMapper.getMarketInfo(curDate, mCode);
            return R.ok(data);
        });
        return result;
    }

    @Override
    public R<PageResult<StockUpDownDomain>> getStockInfoByPage(Integer page, Integer pageSize) {
        //获取股票的最新交易时间点（精确到分钟，秒和毫秒置为0）
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curDate = DateTime.parse("2022-06-07 15:00:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //设置PageHelper分页参数
        PageHelper.startPage(page, pageSize);
        //调用mapper查询
        List<StockUpDownDomain> pageData = stockRtInfoMapper.getStockInfoByTime(curDate);
        //组装PageResult对象
        PageInfo<StockUpDownDomain> pageInfo = new PageInfo<>(pageData);
        PageResult<StockUpDownDomain> pageResult = new PageResult<>(pageInfo);
        return R.ok(pageResult);
    }

    @Override
    public R<List<StockUpDownDomain>> getStockInfoIncrease() {
        //获取股票的最新交易时间点（精确到分钟，秒和毫秒置为0）
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curDate = DateTime.parse("2021-12-30 10:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        List<StockUpDownDomain> curData = stockRtInfoMapper.getStockInfoOrderByUpDown(curDate);
        return R.ok(curData);
    }

    @Override
    public R<Map<String, List>> getStockUpDownCount() {
        //获取股票的最新交易时间点（精确到分钟，秒和毫秒置为0）
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curDateTime = DateTime.parse("2022-01-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate = curDateTime.toDate();
        //获取最近交易时间点的开盘时间
        Date openDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //统计涨停的数量
        List<Map> upList = stockRtInfoMapper.getStockUpDownCount(openDate, endDate, 1);
        //统计跌停的数量
        List<Map> downlist = stockRtInfoMapper.getStockUpDownCount(openDate, endDate, 0);
        //组装数据
        HashMap<String, List> info = new HashMap<>();
        info.put("upList", upList);
        info.put("downList", downlist);
        return R.ok(info);
    }

    @Override
    public void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response) {
        //获取分页数据
        R<PageResult<StockUpDownDomain>> stockInfoByPage = this.getStockInfoByPage(page, pageSize);
        List<StockUpDownDomain> rows = stockInfoByPage.getData().getRows();
        //将数据导出到Excel中
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        try {
            String fileName = URLEncoder.encode("股票信息表", "utf-8");
            response.setHeader("Content-disposition", "attachment;filename" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), StockUpDownDomain.class).sheet("股票涨幅信息").doWrite(rows);
        } catch (IOException e) {
            log.error("当前页码：{}，每页大小：{}，当前时间：{}，异常信息：{}", page, pageSize, DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), e.getMessage());
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            R<Object> error = R.error(ResponseCode.ERROR);
        }

    }

    @Override
    public R<Map<String, List>> getStockTradeAmount() {
        //获取股票的最新交易时间点（精确到分钟，秒和毫秒置为0）
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //mock info 等后续完成股票采集job工程，再将代码删除即可
        curDateTime = DateTime.parse("2022-01-03 14:40:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date tEndDate = curDateTime.toDate();
        //获取最近交易时间点的开盘时间
        Date tOpenDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //获取前一天的交易时间的开盘时间
        DateTime preTEndTime = DateTimeUtil.getPreviousTradingDay(curDateTime);
        preTEndTime = DateTime.parse("2022-01-02 14:40:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date preTEndDate = preTEndTime.toDate();
        Date preTOpenDate = DateTimeUtil.getOpenDate(preTEndTime).toDate();
        //获取大盘交易日的成交量数据
        List<Map> amtList = stockMarketIndexInfoMapper.getStockTradeAmount(tOpenDate, tEndDate, stockInfoConfig.getInner());
        //获取前一天的大盘交易日的成交数据
        List<Map> yesAmtList = stockMarketIndexInfoMapper.getStockTradeAmount(preTOpenDate, preTEndDate, stockInfoConfig.getInner());
        //组装数据，方便返回
        HashMap<String, List> info = new HashMap<>();
        info.put("amtList", amtList);
        info.put("yesAmtList", yesAmtList);
        return R.ok(info);
    }

    @Override
    public R<Map> getIncreaseRangeInfo() {
        //获取股票的最新交易时间点（精确到分钟，秒和毫秒置为0）
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //mock info 等后续完成股票采集job工程，再将代码删除即可
        curDateTime = DateTime.parse("2022-01-06 09:55:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate = curDateTime.toDate();
        //获取各个涨幅区间的A股的数据
        List<Map> infos = stockRtInfoMapper.getIncreaseRangeData(curDate);
        List<String> upDownRange = stockInfoConfig.getUpDownRange();
        //将涨幅区间的每个元素转化为Map对象
        //方式1：普通循环
//        List<Map> allInfos = new ArrayList<>();
//        for (String title : upDownRange) {
//            Map tmp = null;
//            for (Map info : infos) {
//                if (info.containsValue(title)) {
//                    tmp = info;
//                    break;
//                }
//            }
//            if (tmp ==null){
//                tmp=new HashMap<>();
//                tmp.put("count",0);
//                tmp.put("title",title);
//            }
//            allInfos.add(tmp);
//        }
        //方式2：lambda表达式
        List<Object> allInfos = upDownRange.stream().map(title -> {
            Optional<Map> result = infos.stream().filter(map -> map.containsValue(title)).findFirst();
            if (result.isPresent()) {
                return result.get();
            } else {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp.put("count", 0);
                tmp.put("title", title);
                return tmp;
            }
        }).collect(Collectors.toList());
        //组装数据，方便返回
        HashMap<String, Object> data = new HashMap<>();
        data.put("time", curDateTime.toString("yyyy-MM-dd HH:mm:ss"));
//        data.put("infos", infos);
        data.put("infos", allInfos);
        return R.ok(data);
    }

    @Override
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode) {
        //获取当前交易日的时间点
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //mockData
        curDateTime = DateTime.parse("2021-12-30 14:30:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate = curDateTime.toDate();
        Date openDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //获取当前交易日的分时数据
        List<Stock4MinuteDomain> data = stockRtInfoMapper.getStock4MinuteInfo(openDate, curDate, stockCode);
        //判断非空处理
        if (CollectionUtils.isEmpty(data)) {
            data=new ArrayList<>();
        }
        //返回数据
        return R.ok(data);
    }

    @Override
    public R<List<Stock4EvrDayDomain>> getStockScreenDkLine(String stockCode) {
        //根据当前日期获取最近交易日日期
        DateTime curTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //MOCK DATA
        curTime=DateTime.parse("2022-06-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate = curTime.toDate();
        //获取起始时间
        DateTime startTime =curTime.minusMonths(3);
        startTime=DateTime.parse("2022-01-01 9:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date startDate = startTime.toDate();
        //根据股票代码和时间段获取股票日K数据
        List<Stock4EvrDayDomain> dkLineData=stockRtInfoMapper.getStock4DkLine(startDate,curDate,stockCode);
        //返回数据
        return R.ok(dkLineData);
    }

    @Override
    public R<List<Stock4OuterDomain>> getOuterMarketInfo() {
        //根据交易日的时间点
        List<Stock4OuterDomain> listData=stockOuterMarketIndexInfoMapper.getOuterMarketInfo();
        return R.ok(listData);
    }

    @Override
    public R<List<Stock4SearchDomain>> getStockSearchStr(String searchStr) {
        //根据输入的searchStr的股票代码模糊查找
        List<Stock4SearchDomain> searchData=stockRtInfoMapper.getStockSearchStr(searchStr);
        return R.ok(searchData);
    }

    @Override
    public R<Stock4DescribeDomain> getStockDescribe(String stockCode) {
        //根据股票代码获取股票描述
        Stock4DescribeDomain desData= stockBusinessMapper.getStockDescribe(stockCode);
        return R.ok(desData);
    }

}
