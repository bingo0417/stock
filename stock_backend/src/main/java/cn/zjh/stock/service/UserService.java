package cn.zjh.stock.service;

import cn.zjh.stock.pojo.vo.req.LoginReqVo;
import cn.zjh.stock.pojo.vo.resp.LoginRespVo;
import cn.zjh.stock.pojo.vo.resp.R;
import cn.zjh.stock.pojo.entity.SysUser;

import java.util.Map;

/**
 * @ClassName UserService
 * @Description //用户服务层接口
 * @Author Rum
 * @Date 2024/7/11 15:57
 * @Version
 **/
public interface UserService {
    SysUser findUserInfoByUsername(String userName);

    R<LoginRespVo> login(LoginReqVo loginReqVo);

    R<Map> getCaptchaCode();
}
