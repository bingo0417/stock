package cn.zjh.stock.pojo.vo.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName LoginReqVo
 * @Description //请求登录vo
 * @Author Rum
 * @Date 2024/7/12 13:53
 * @Version
 **/
@Data
@Schema(description = "登录请求vo")
public class LoginReqVo {
    @Schema(description = "用户名")
    private String username;
    @Schema(description = "密码")
    private String password;
    @Schema(description = "验证码")
    private String code;
    @Schema(description = "sessionId")
    private String sessionId;
}
