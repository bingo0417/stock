package cn.zjh.stock.controller;

import cn.zjh.stock.pojo.domain.*;
import cn.zjh.stock.pojo.vo.resp.PageResult;
import cn.zjh.stock.pojo.vo.resp.R;
import cn.zjh.stock.service.StockService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @ClassName StockController
 * @Description 股票相关接口控制器
 * @Author Rum
 * @Date 2024/7/16 17:05
 * @Version
 **/
@Tag(name = "股票相关接口控制器", description = "股票相关接口控制器")
@RestController
@RequestMapping("/api/quot")
public class StockController {
    @Autowired
    private StockService stockService;

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.InnerMarketDomain>>
     * @Author Rum
     * @Description 获取国内最新的大盘数据
     * @Date 17:15 2024/7/16
     * @Param []
     **/
    @Operation(summary = "获取国内最新的大盘数据", description = "获取国内最新的大盘数据")
    @GetMapping("/index/all")
    public R<List<InnerMarketDomain>> getInnerMarket() {
        return stockService.getInnerMarket();
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<cn.zjh.stock.pojo.vo.resp.PageResult < cn.zjh.stock.pojo.domain.StockUpDownDomain>>
     * @Author Rum
     * @Description 分页查询最新的股票交易数据
     * @Date 15:58 2024/7/17
     * @Param [page, pageSize]
     **/
    @Parameters({
            @Parameter(name = "page", description = "", in = ParameterIn.QUERY),
            @Parameter(name = "pageSize", description = "", in = ParameterIn.QUERY)
    })
    @Operation(summary = "分页查询最新的股票交易数据", description = "分页查询最新的股票交易数据")
    @GetMapping("/stock/all")
    public R<PageResult<StockUpDownDomain>> getStockInfoByPage(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                               @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        return stockService.getStockInfoByPage(page, pageSize);
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.StockUpDownDomain>>
     * @Author Rum
     * @Description 查询显示最新的涨幅前四的股票交易数据
     * @Date 19:39 2024/7/17
     * @Param []
     **/
    @Operation(summary = "查询显示最新的涨幅前四的股票交易数据", description = "查询显示最新的涨幅前四的股票交易数据")
    @GetMapping("/stock/increase")
    public R<List<StockUpDownDomain>> getStockInfoIncrease() {
        return stockService.getStockInfoIncrease();
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.Map < java.lang.String, java.util.List>>
     * @Author Rum
     * @Description 统计股票交易日内每分钟的涨跌停的股票数量
     * @Date 21:03 2024/7/17
     * @Param []
     **/
    @Operation(summary = "统计股票交易日内每分钟的涨跌停的股票数量", description = "统计股票交易日内每分钟的涨跌停的股票数量")
    @GetMapping("/stock/updown/count")
    public R<Map<String, List>> getStockUpDownCount() {
        return stockService.getStockUpDownCount();
    }

    /**
     * @return void
     * @Author Rum
     * @Description 导出指定页码的最新股票信息
     * @Date 16:23 2024/7/18
     * @Param [page, pageSize, response]
     **/
    @Parameters({
            @Parameter(name = "page", description = "", in = ParameterIn.QUERY),
            @Parameter(name = "pageSize", description = "", in = ParameterIn.QUERY)
    })
    @Operation(summary = "导出指定页码的最新股票信息", description = "导出指定页码的最新股票信息")
    @GetMapping("/stock/export")
    public void exportStockUpDownInfo(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                      @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                                      HttpServletResponse response) {
        stockService.exportStockUpDownInfo(page, pageSize, response);
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.Map < java.lang.String, java.util.List < java.util.Map>>>
     * @Author Rum
     * @Description 统计大盘交易日与前一天的成交量对比
     * @Date 19:49 2024/7/18
     * @Param []
     **/
    @Operation(summary = "统计大盘交易日与前一天的成交量对比", description = "统计大盘交易日与前一天的成交量对比")
    @GetMapping("/stock/tradeAmt")
    public R<Map<String, List>> getStockTradeAmount() {
        return stockService.getStockTradeAmount();
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.Map>
     * @Author Rum
     * @Description 统计A股在交易日各个涨幅区间的数据
     * @Date 13:56 2024/7/19
     * @Param []
     **/
    @Operation(summary = "统计A股在交易日各个涨幅区间的数据", description = "统计A股在交易日各个涨幅区间的数据")
    @GetMapping("/stock/updown")
    public R<Map> getIncreaseRangeInfo() {
        return stockService.getIncreaseRangeInfo();
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.Stock4MinuteDomain>>
     * @Author Rum
     * @Description 统计个股分时的行情数据
     * @Date 16:29 2024/7/19
     * @Param [stockCode]
     **/
    @Parameter(name = "stockCode", description = "", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "统计个股分时的行情数据", description = "统计个股分时的行情数据")
    @GetMapping("/stock/screen/time-sharing")
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(@RequestParam(value = "code", required = true) String stockCode) {
        return stockService.getStockScreenTimeSharing(stockCode);
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.Stock4EvrDayDomain>>
     * @Author Rum
     * @Description 查询个股单日K线数据
     * @Date 20:50 2024/7/19
     * @Param [stockCode]
     **/
    @Parameter(name = "stockCode", description = "", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "查询个股单日K线数据", description = "查询个股单日K线数据")
    @GetMapping("/stock/screen/dkline")
    public R<List<Stock4EvrDayDomain>> getStockScreenDkLine(@RequestParam(value = "code", required = true) String stockCode) {
        return stockService.getStockScreenDkLine(stockCode);
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.Stock4OuterDomain>>
     * @Author Rum
     * @Description 外盘指数行情数据查询前四名
     * @Date 11:26 2024/7/23
     * @Param []
     **/
    @Operation(summary = "外盘指数行情数据查询前四名", description = "外盘指数行情数据查询前四名")
    @GetMapping("/external/index")
    public R<List<Stock4OuterDomain>> getOuterMarketInfo() {
        return stockService.getOuterMarketInfo();
    }

    /**
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List < cn.zjh.stock.pojo.domain.Stock4SearchDomain>>
     * @Author Rum
     * @Description 股票code联想推荐
     * @Date 19:51 2024/7/23
     * @Param [searchStr]
     **/
    @Parameter(name = "searchStr", description = "", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "股票code联想推荐", description = "股票code联想推荐")
    @GetMapping("/stock/search")
    public R<List<Stock4SearchDomain>> getStockSearchStr(@RequestParam(value = "searchStr", required = true) String searchStr) {
        return stockService.getStockSearchStr(searchStr);
    }

    /**
     * @Author Rum
     * @Description 根据代码获取股票业务描述
     * @Date 10:33 2024/7/24
     * @Param [stockCode]
     * @return cn.zjh.stock.pojo.vo.resp.R<cn.zjh.stock.pojo.domain.Stock4DescribeDomain>
     **/
    @Parameter(name = "stockCode", description = "", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "根据代码获取股票业务描述", description = "根据代码获取股票业务描述")
    @GetMapping("/stock/describe")
    public R<Stock4DescribeDomain> getStockDescribe(@RequestParam(value = "code",required = true) String stockCode){
        return stockService.getStockDescribe(stockCode);
    }

    /**
     * @Author Rum
     * @Description //TODO 统计指定股票代码的周k线数据
     * @Date 12:50 2024/7/24
     * @Param [stockCode]
     * @return cn.zjh.stock.pojo.vo.resp.R<java.util.List<cn.zjh.stock.pojo.domain.Stock4WeekklineDomain>>
     **/
    @Parameter(name = "code", description = "股票代码", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "统计指定股票代码的周k线数据", description = "统计指定股票代码的周k线数据")
    @GetMapping("/stock/screen/weekkline")
    public R<List<Stock4WeekklineDomain>> getStockWeekkline(@RequestParam(value = "code",required = true) String stockCode){
        return null;
    }
}
