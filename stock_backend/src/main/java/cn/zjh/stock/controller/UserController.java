package cn.zjh.stock.controller;

import cn.zjh.stock.pojo.vo.req.LoginReqVo;
import cn.zjh.stock.pojo.vo.resp.LoginRespVo;
import cn.zjh.stock.pojo.vo.resp.R;
import cn.zjh.stock.pojo.entity.SysUser;
import cn.zjh.stock.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @ClassName UserController
 * @Description //用户控制层
 * @Author Rum
 * @Date 2024/7/11 16:03
 * @Version
 **/
@Tag(name = "用户控制层", description = "用户控制层")
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserService userService;

    @Parameter(name = "userName", description = "", in = ParameterIn.PATH, required = true)
    @Operation(summary = "根据用户名查询用户信息", description = "根据用户名查询用户信息")
    @GetMapping("/user/{userName}")
    public SysUser findUserByUsername(@PathVariable String userName) {
        return userService.findUserInfoByUsername(userName);
    }

    @Operation(summary = "用户登录", description = "用户登录")
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo loginReqVo) {
        return userService.login(loginReqVo);
    }

    @Operation(summary = "获取验证码", description = "获取验证码")
    @GetMapping("/captcha")
    public R<Map> getCaptchaCode(){
        return userService.getCaptchaCode();
    }

}
