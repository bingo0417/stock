package cn.zjh.stock.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MqConfig
 * @Description 定义rabbitMQ消息队列的相关配置
 * @Author Rum
 * @Date 2024/7/21 16:20
 * @Version
 **/
@Configuration
public class MqConfig {
    /**
     * @Author Rum
     * @Description 重新定义消息序列化的方式，改为基于json格式序列化和反序列化
     * @Date 15:51 2024/7/21
     * @Param []
     * @return org.springframework.amqp.support.converter.MessageConverter
     **/
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

//    /**
//     * @Author Rum
//     * @Description 定义路由股票信息的交换机
//     * @Date 15:51 2024/7/21
//     * @Param []
//     * @return org.springframework.amqp.core.TopicExchange
//     **/
//    @Bean
//    public TopicExchange stockTopicExchange(){
//        return new TopicExchange("stockExchange",true,false);
//    }
//
//    /**
//     * @Author Rum
//     * @Description 创建国内大盘信息队列
//     * @Date 15:53 2024/7/21
//     * @Param []
//     * @return org.springframework.amqp.core.Queue
//     **/
//    @Bean
//    public Queue innerQueue(){
//        return new Queue("innerMarketQueue",true);
//    }
//
//    /**
//     * @Author Rum
//     * @Description 绑定国内大盘的队列到股票主题交换机
//     * @Date 15:57 2024/7/21
//     * @Param []
//     * @return org.springframework.amqp.core.Binding
//     **/
//    @Bean
//    public Binding bindingInnerQueue(){
//        return BindingBuilder.bind(innerQueue()).to(stockTopicExchange()).with("inner.market");
//    }


}
