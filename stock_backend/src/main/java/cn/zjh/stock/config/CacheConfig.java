package cn.zjh.stock.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @ClassName RedisCacheConfig
 * @Description 自定义redis序列化方式，避免默认使用jdk的序列化方式（jdk序列化阅读体验差，序列化后占用较大的体积，占用过多内存）
 * @Author Rum
 * @Date 2024/7/13 14:40
 * @Version
 **/
@Configuration
public class CacheConfig {
    /**
     * @Author Rum
     * @Description 自定义模板对象进行注入Ioc容器，bean命名一定是RedisTemplate
     * @Date 15:01 2024/7/13
     * @Param [redisConnectionFactory]
     * @return org.springframework.data.redis.core.RedisTemplate
     **/
    @Bean
    public RedisTemplate redisTemplate(@Autowired RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //2.为不同的数据结构设置不同的序列化方案
        //设置key序列化方式
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //设置value序列化方式
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        //设置hash中field字段序列化方式
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        //设置hash中value的序列化方式
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        //5.初始化参数设置
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
    /**
     * @Author Rum
     * @Description 构建缓存bean
     * @Date 16:47 2024/7/21
     * @Param []
     * @return com.github.benmanes.caffeine.cache.Cache<java.lang.String,java.lang.Object>
     **/
    @Bean
    public Cache<String,Object> caffeineCache(){
        Cache<String, Object> cache = Caffeine
                .newBuilder()
                .maximumSize(200)//设置缓存数量上限
//                .expireAfterAccess(1, TimeUnit.SECONDS)//访问1秒后删除
//                .expireAfterWrite(1,TimeUnit.SECONDS)//写入1秒后删除
                .initialCapacity(100)// 初始的缓存空间大小
                .recordStats()//开启统计
                .build();
        return cache;
    }
}
