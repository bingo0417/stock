package cn.zjh.stock.config;

import cn.zjh.stock.pojo.vo.StockInfoConfig;
import cn.zjh.stock.utils.IdWorker;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @ClassName CommonConfig
 * @Description //公共配置类
 * @Author Rum
 * @Date 2024/7/12 14:06
 * @Version
 **/
@Configuration
@EnableConfigurationProperties({StockInfoConfig.class })//开启相关对象的加载
public class CommonConfig {
    /**
     * 密码加密、匹配bean
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public IdWorker idWorker(){
        return new IdWorker(1l, 2l);
    }
}
