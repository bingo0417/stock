package cn.zjh.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName PasswordEncoderTest
 * @Description //密码加密测试
 * @Author Rum
 * @Date 2024/7/12 14:13
 * @Version
 **/
@SpringBootTest
public class PasswordEncoderTest {
    @Autowired
    private PasswordEncoder passwordEncoder;
    /**
     * @Author Rum
     * @Description //测试密码加密
     * @Date 14:18 2024/7/12
     * @Param []
     * @return void
     **/
    @Test
    public void test01() {
        String password = "123456";
        String encode = passwordEncoder.encode(password);
        // $2a$10$jqG7WURj9DlUFAHgFTlnF.ZeLYbIas.EpvxC2piMeyJ02GhL3WLOG
        System.out.println(encode);
    }
    /**
     * @Author Rum
     * @Description //测试密码匹配
     * @Date 14:19 2024/7/12
     * @Param []
     * @return void
     **/
    @Test
    public void test02() {
        String password = "123456";
        String encode ="$2a$10$jqG7WURj9DlUFAHgFTlnF.ZeLYbIas.EpvxC2piMeyJ02GhL3WLOG";
        System.out.println(passwordEncoder.matches(password,encode));
    }
}
