package cn.zjh.stock;

import cn.zjh.stock.pojo.User;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName EasyExcelTest
 * @Description easyExcel测试类
 * @Author Rum
 * @Date 2024/7/18 15:20
 * @Version
 **/
public class EasyExcelTest {
    public List<User> init() {
        //组装数据
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setAddress("上海" + i);
            user.setUserName("张三" + i);
            user.setBirthday(new Date());
            user.setAge(10 + i);
            users.add(user);
        }
        return users;
    }

    /**
     * @return
     * @Author Rum
     * @Description 导出测试
     * @Date 15:24 2024/7/18
     * @Param
     **/
    @Test
    public void testWrite() {
        List<User> users = init();
        EasyExcel.write("C:\\Users\\Rum\\Desktop\\data\\test.xls", User.class).sheet("用户信息").doWrite(users);
    }

    /**
     * @return void
     * @Author Rum
     * @Description 测试读取Excel数据
     * @Date 15:54 2024/7/18
     * @Param []
     **/
    @Test
    public void testRead() {
        List<User> users = new ArrayList<>();
        EasyExcel.read("C:\\Users\\Rum\\Desktop\\data\\test.xls", User.class, new AnalysisEventListener<User>() {
            /**
             * @Author Rum
             * @Description 逐行读取数据的操作
             * @Date 15:59 2024/7/18
             * @Param [data, analysisContext]
             * @return void
             **/
            @Override
            public void invoke(User data, AnalysisContext analysisContext) {
                users.add(data);
            }

            /**
             * @Author Rum
             * @Description 读取完毕后的通知
             * @Date 15:59 2024/7/18
             * @Param [analysisContext]
             * @return void
             **/
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("Excel read finished!");
            }
        }).sheet("用户信息").doRead();
    }
}
