package cn.zjh.stock.utils;

import cn.zjh.stock.mapper.SysUserMapper;
import cn.zjh.stock.pojo.entity.SysUser;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @ClassName PageHelperTest
 * @Description 分页测试类
 * @Author Rum
 * @Date 2024/7/17 14:30
 * @Version
 **/
@SpringBootTest
public class PageHelperTest {
    @Autowired
    private SysUserMapper  sysUserMapper;

    @Test
    public void test01(){
        Integer page=2;
        Integer pageSize=5;
        PageHelper.startPage(page,pageSize);
        List<SysUser> all = sysUserMapper.findAll();
        PageInfo<SysUser> pageInfo=new PageInfo<>(all);
        int pageNum=pageInfo.getPageNum();
        int pages=pageInfo.getPages();
        int size=pageInfo.getSize();
        int pageSize0=pageInfo.getPageSize();
        long total = pageInfo.getTotal();
        List<SysUser> list = pageInfo.getList();
        System.out.println(pageNum);
        System.out.println(pages);
        System.out.println(size);
        System.out.println(total);
        System.out.println(pageSize0);
        System.out.println(all);
    }

}
