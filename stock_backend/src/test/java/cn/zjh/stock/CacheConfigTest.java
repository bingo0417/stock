package cn.zjh.stock;

import cn.zjh.stock.mq.StockMQMsgListener;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @ClassName RedisCacheConfigTest
 * @Description //Redis测试类
 * @Author Rum
 * @Date 2024/7/13 16:50
 * @Version
 **/
@SpringBootTest
public class CacheConfigTest {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Test
    public void test() {
        redisTemplate.opsForValue().set("name", "张嘉灏");
        String name = redisTemplate.opsForValue().get("name");
        System.out.println(name);
    }

}
