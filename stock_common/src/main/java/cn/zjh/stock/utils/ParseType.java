package cn.zjh.stock.utils;

/**
 * @ClassName DateTimeUtil
 * @Description 股票信息采集数据类型标识
 * @Author Rum
 * @Date 2024/7/21 10:14
 * @Version
 **/
public class ParseType {

    /**
     * A股大盘标识
     */
    public static final int INNER=1;

    /**
     * 国外大盘标识
     */
    public static final int OUTER=2;

    /**
     * A股标识
     */
    public static final int ASHARE=3;

}