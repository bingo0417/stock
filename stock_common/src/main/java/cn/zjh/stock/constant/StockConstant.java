package cn.zjh.stock.constant;

/**
 * @ClassName StockConstant
 * @Description //常量信息封装
 * @Author Rum
 * @Date 2024/7/14 11:59
 * @Version
 **/
public class StockConstant {
    /**
     * @Author Rum
     * @Description //校验码前缀
     * @Date 12:01 2024/7/14
     * @Param
     * @return
     **/
    public static final String CHECK_PREFIX="CK:";
    /**
     * @Author Rum
     * @Description //http请求头携带的Token信息key
     * @Date 12:01 2024/7/14
     * @Param
     * @return
     **/
    public static final String TOKEN_HEADER = "authorization";
    /**
     * @Author Rum
     * @Description //缓存股票相关信息的cacheName的命名前缀
     * @Date 12:04 2024/7/14
     * @Param
     * @return
     **/
    public static final String STOCK="stock";
}
