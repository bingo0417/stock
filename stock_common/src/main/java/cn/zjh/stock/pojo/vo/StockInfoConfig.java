package cn.zjh.stock.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Schema
@ConfigurationProperties(prefix = "stock")
@Data
public class StockInfoConfig {
    //A股大盘ID集合
    @Schema(description = "A股大盘ID集合")
    private List<String> inner;
    //外盘ID集合
    @Schema(description = "外盘ID集合")
    private List<String> outer;
    //涨幅区间集合
    @Schema(description = "涨幅区间集合")
    private List<String> upDownRange;
    //大盘外盘个股的公共URL
    @Schema(description = "大盘外盘个股的公共URL")
    private String marketUrl;
    //板块采集URL
    @Schema(description = "板块采集URL")
    private String blockUrl;
}