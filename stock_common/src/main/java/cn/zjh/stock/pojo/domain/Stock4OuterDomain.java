package cn.zjh.stock.pojo.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @ClassName Stock4OuterDomain
 * @Description 响应外盘指数的Domain
 * @Author Rum
 * @Date 2024/7/23 17:23
 * @Version
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "响应外盘指数的Domain")
public class Stock4OuterDomain {
    /**
     * 大盘名称
     */
    @Schema(description = "大盘名称")
    private String name;

    /**
     * 大盘当前点
     */
    @Schema(description = "大盘当前点")
    private BigDecimal curPoint;

    /**
     * 大盘涨跌值
     */
    @Schema(description = "大盘涨跌值")
    private BigDecimal upDown;

    /**
     * 大盘涨幅
     */
    @Schema(description = "大盘涨幅")
    private BigDecimal rose;

    /**
     * 当前时间
     */
    @Schema(description = "当前时间")
    private String curTime;
}
