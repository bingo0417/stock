package cn.zjh.stock.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户角色表
 * @TableName sys_user_role
 */
@Data
@Schema(description = "用户角色表")
public class SysUserRole implements Serializable {
    /**
     * 主键
     */
    @Schema(description = "主键")
    private Long id;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private Long userId;

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    private Long roleId;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}