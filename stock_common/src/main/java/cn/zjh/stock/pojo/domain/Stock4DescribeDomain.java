package cn.zjh.stock.pojo.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName Stock4DescribeDomain
 * @Description 个股主营业务Domain
 * @Author Rum
 * @Date 2024/7/24 10:26
 * @Version
 **/
@Schema(description = "个股主营业务Domain")
@Data
public class Stock4DescribeDomain {
    /**
     * 股票代码
     */
    @Schema(description = "股票代码")
    private String code;
    /**
     * 行业板块名称
     */
    @Schema(description = "行业板块名称")
    private String trade;
    /**
     * 主营业务
     */
    @Schema(description = "主营业务")
    private String business;
    /**
     * 股票名称
     */
    @Schema(description = "股票名称")
    private String name;
}
