package cn.zjh.stock.pojo.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName Stock4WeekklineDomain
 * @Description 统计每周内的股票数据信息
 * @Author Rum
 * @Date 2024/7/24 12:08
 * @Version
 **/
@Schema(description = "统计每周内的股票数据信息")
public class Stock4WeekklineDomain {
    /**
     * 周平均股价
     */
    @Schema(description = "周平均股价")
    private BigDecimal avgPrice;
    /**
     * 周最低价
     */
    @Schema(description = "周最低价")
    private BigDecimal minPrice;
    /**
     * 周一开盘价
     */
    @Schema(description = "周一开盘价")
    private BigDecimal openPrice;
    /**
     * 周最高价
     */
    @Schema(description = "周最高价")
    private BigDecimal maxPrice;
    /**
     * 周收盘价
     */
    @Schema(description = "周收盘价")
    private BigDecimal closePrice;
    /**
     * 周最大时间
     */
    @Schema(description = "周最大时间")
    private Data mxTime;
    /**
     * 股票代码
     */
    @Schema(description = "股票代码")
    private String stockCode;
}
