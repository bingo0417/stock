package cn.zjh.stock.pojo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
/**
 * @ClassName Stock4MinuteDomain
 * @Description 封装股票K线数据实体类
 * @Author Rum
 * @Date 2024/7/19 20:23
 * @Version
 **/
@Schema(description = "封装股票K线数据实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Stock4EvrDayDomain {
    /**
     * 日期，eg:202201280809
     */
   @Schema(description = "日期，eg:202201280809")
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
   private Date date;
    /**
     * 交易量
     */
   @Schema(description = "交易量")
   private Long tradeAmt;
    /**
     * 股票编码
     */
   @Schema(description = "股票编码")
   private String code;
    /**
     * 最低价
     */
   @Schema(description = "最低价")
   private BigDecimal lowPrice;
    /**
     * 股票名称
     */
    @Schema(description = "股票名称")
    private String name;
    /**
     * 最高价
     */
    @Schema(description = "最高价")
    private BigDecimal highPrice;
    /**
     * 开盘价
     */
    @Schema(description = "开盘价")
    private BigDecimal openPrice;
    /**
     * 当前交易总金额
     */
    @Schema(description = "当前交易总金额")
    private BigDecimal tradeVol;
    /**
     * 当前收盘价格指收盘时的价格，如果当天未收盘，则显示最新cur_price）
     */
    @Schema(description = "当前收盘价格指收盘时的价格，如果当天未收盘，则显示最新cur_price）")
    private BigDecimal closePrice;
    /**
     * 前收盘价
     */
   @Schema(description = "前收盘价")
   private BigDecimal preClosePrice;
}