package cn.zjh.stock.pojo.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName Stock4SearchDomain
 * @Description 模糊查询Domain
 * @Author Rum
 * @Date 2024/7/23 19:45
 * @Version
 **/
@Schema(description = "模糊查询Domain")
@Data
public class Stock4SearchDomain {
    /**
     * 股票编码
     */
    @Schema(description = "股票编码")
    private String code;
    /**
     * 股票名称
     */
    @Schema(description = "股票名称")
    private String name;
}
