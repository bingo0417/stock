package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.domain.InnerMarketDomain;
import cn.zjh.stock.pojo.entity.StockMarketIndexInfo;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Rum
* @description 针对表【stock_market_index_info(国内大盘数据详情表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.StockMarketIndexInfo
*/
public interface StockMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockMarketIndexInfo record);

    int insertSelective(StockMarketIndexInfo record);

    StockMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockMarketIndexInfo record);

    int updateByPrimaryKey(StockMarketIndexInfo record);

    /**
     * @Author Rum
     * @Description 根据指定的时间点以及大盘编码查询对应的数据
     * @Date 17:49 2024/7/16
     * @Param [curDate, marketCode]
     * @return java.util.List<cn.zjh.stock.pojo.domain.InnerMarketDomain>
     **/
    List<InnerMarketDomain> getMarketInfo(@Param("curDate") Date curDate, @Param("marketCode") List<String> marketCode);

    /**
     * @Author Rum
     * @Description 统计指定日期范围内的指定大盘每分钟的成交量流水信息
     * @Date 20:58 2024/7/18
     * @Param [OpenDate, EndDate, marketCodes]
     * @return java.util.List<java.util.Map>
     **/
    List<Map> getStockTradeAmount(@Param("OpenDate") Date OpenDate, @Param("EndDate") Date EndDate, @Param("marketCodes") List<String> marketCodes);

    /**
     * @Author Rum
     * @Description 往数据库大盘实体注入对象集合
     * @Date 19:40 2024/7/20
     * @Param [entities]
     * @return int
     **/
    int insertBatch(@Param("infos") List<StockMarketIndexInfo> entities);
}
