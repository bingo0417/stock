package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.entity.SysUser;

import java.util.List;

/**
* @author Rum
* @description 针对表【sys_user(用户表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.SysUser
*/
public interface SysUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);
    SysUser selectUserInfoByUsername(String userName);
    /**
     *查询所有的用户信息
     */
    List<SysUser> findAll();

}
