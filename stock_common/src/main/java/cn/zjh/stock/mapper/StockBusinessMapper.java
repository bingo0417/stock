package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.domain.Stock4DescribeDomain;
import cn.zjh.stock.pojo.entity.StockBusiness;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Rum
* @description 针对表【stock_business(主营业务表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.StockBusiness
*/
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);

    int updateByPrimaryKey(StockBusiness record);

    /**
     * @Author Rum
     * @Description 获取所有的A股的编码的集合
     * @Date 20:23 2024/7/20
     * @Param []
     * @return java.util.List<java.lang.String>
     **/
    List<String> getAllByStockCodes();

    /**
     * @Author Rum
     * @Description 根据股票代码获取股票描述
     * @Date 10:55 2024/7/24
     * @Param [stockCode]
     * @return cn.zjh.stock.pojo.domain.Stock4DescribeDomain
     **/
    Stock4DescribeDomain getStockDescribe(@Param("stockCode") String stockCode);
}
