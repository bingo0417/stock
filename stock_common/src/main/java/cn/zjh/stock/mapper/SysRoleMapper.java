package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.entity.SysRole;

/**
* @author Rum
* @description 针对表【sys_role(角色表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.SysRole
*/
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

}
