package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.domain.Stock4OuterDomain;
import cn.zjh.stock.pojo.entity.StockOuterMarketIndexInfo;

import java.util.List;

/**
* @author Rum
* @description 针对表【stock_outer_market_index_info(外盘详情信息表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.StockOuterMarketIndexInfo
*/
public interface StockOuterMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockOuterMarketIndexInfo record);

    int insertSelective(StockOuterMarketIndexInfo record);

    StockOuterMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockOuterMarketIndexInfo record);

    int updateByPrimaryKey(StockOuterMarketIndexInfo record);

    /**
     * @return java.util.List<cn.zjh.stock.pojo.entity.StockOuterMarketIndexInfo>
     * @Author Rum
     * @Description 外盘指数行情数据查询前四名
     * @Date 17:08 2024/7/23
     * @Param []
     **/
    List<Stock4OuterDomain> getOuterMarketInfo();
}
