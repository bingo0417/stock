package cn.zjh.stock.mapper;

import cn.zjh.stock.pojo.domain.*;
import cn.zjh.stock.pojo.entity.StockRtInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Rum
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2024-07-11 14:09:35
* @Entity cn.zjh.stock.pojo.entity.StockRtInfo
*/
@Schema(description = "针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper")
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    /**
     * @Author Rum
     * @Description 查询指定时间点下的股票数据集合
     * @Date 17:18 2024/7/17
     * @Param [curDate]
     * @return java.util.List<cn.zjh.stock.pojo.domain.StockUpDownDomain>
     **/
    List<StockUpDownDomain> getStockInfoByTime(@Param("curDate") Date curDate);

    /**
     * @Author Rum
     * @Description 查询涨幅前四名最新时间的股票数据集合
     * @Date 19:45 2024/7/17
     * @Param [curDate]
     * @return java.util.List<cn.zjh.stock.pojo.domain.StockUpDownDomain>
     **/
    List<StockUpDownDomain> getStockInfoOrderByUpDown(@Param("curDate") Date curDate);

    /**
     * @Author Rum
     * @Description 统计涨停跌停统计的股票数据
     * @Date 10:32 2024/7/18
     * @Param [openDate, endDate, flag]
     * @return java.util.List<java.util.Map>
     **/
    List<Map> getStockUpDownCount(@Param("openDate") Date openDate, @Param("endDate") Date endDate, @Param("flag") int flag);

    /**
     * @Author Rum
     * @Description 统计最近交易时间各个涨幅区间的A股的数据
     * @Date 14:09 2024/7/19
     * @Param []
     * @return java.util.List<java.util.Map>
     **/
    List<Map> getIncreaseRangeData(@Param("curDate") Date curDate);

    /**
     * @Author Rum
     * @Description 根据股票编码查询指定时间范围内的分时数据
     * @Date 17:18 2024/7/19
     * @Param [openDate, curDate, stockCode]
     * @return java.util.List<cn.zjh.stock.pojo.domain.Stock4MinuteDomain>
     **/
    List<Stock4MinuteDomain> getStock4MinuteInfo(@Param("openDate") Date openDate, @Param("curDate") Date curDate, @Param("stockCode") String stockCode);

    /**
     * @Author Rum
     * @Description 根据股票代码以及时间范围获取每日K线数据
     * @Date 21:03 2024/7/19
     * @Param [startDate, curDate, stockCode]
     * @return java.util.List<cn.zjh.stock.pojo.domain.Stock4EvrDayDomain>
     **/
    List<Stock4EvrDayDomain> getStock4DkLine(@Param("startDate") Date startDate, @Param("curDate") Date curDate, @Param("stockCode") String stockCode);

    /**
     * @Author Rum
     * @Description 批量插入个股数据
     * @Date 11:29 2024/7/21
     * @Param [list]
     * @return int
     **/
    int insertBatch(@Param("list") List<StockRtInfo> list);

    /**
     * @Author Rum
     * @Description 根据代码模糊查询股票代码、名字
     * @Date 20:16 2024/7/23
     * @Param [searchStr]
     * @return java.util.List<cn.zjh.stock.pojo.domain.Stock4SearchDomain>
     **/
    List<Stock4SearchDomain> getStockSearchStr(@Param("searchStr") String searchStr);

}
