package cn.zjh.stock.service.impl;

import cn.zjh.stock.mapper.StockBusinessMapper;
import cn.zjh.stock.mapper.StockMarketIndexInfoMapper;
import cn.zjh.stock.mapper.StockRtInfoMapper;
import cn.zjh.stock.pojo.entity.StockMarketIndexInfo;
import cn.zjh.stock.pojo.entity.StockRtInfo;
import cn.zjh.stock.pojo.vo.StockInfoConfig;
import cn.zjh.stock.service.StockTimerTaskService;
import cn.zjh.stock.utils.DateTimeUtil;
import cn.zjh.stock.utils.IdWorker;
import cn.zjh.stock.utils.ParseType;
import cn.zjh.stock.utils.ParserStockInfoUtil;
import com.google.common.collect.Lists;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @ClassName StockTimerTaskServiceImpl
 * @Description 股票数据采集业务层
 * @Author Rum
 * @Date 2024/7/20 14:56
 * @Version
 **/
@Service
@Slf4j
public class StockTimerTaskServiceImpl implements StockTimerTaskService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private StockInfoConfig stockInfoConfig;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @Autowired
    private StockBusinessMapper stockBusinessMapper;
    @Autowired
    private ParserStockInfoUtil parserStockInfoUtil;
    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    public HttpEntity<Object> httpEntity;

    @Override
    public void getInnerMarketInfo() {
        //1：采集原始数据
        //组装Url地址
        String url = stockInfoConfig.getMarketUrl() + String.join(",", stockInfoConfig.getInner());
        //维护请求头，添加防盗链和用户标识
        HttpHeaders headers = new HttpHeaders();
        //防盗链
        headers.add("Referer", "https://finance.sina.com.cn/stock/");
        //用户标识
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0");
        //维护Http请求实体对象
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        //发起请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
        int codeValue = responseEntity.getStatusCodeValue();
        if (codeValue != 200) {
            //当前请求失败
            log.error("当前时间点：{}，采集大盘数据失败，状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), codeValue);
            return;
        }
        //获取原始数据
        String jsData = responseEntity.getBody();
        //2：利用Java正则解析原始数据
        //2.1：定义正则表达式
        String reg = "var hq_str_(.+)=\"(.+)\";";
        //2.2：表达式编译
        Pattern pattern = Pattern.compile(reg);
        //2.3：匹配字符串
        Matcher matcher = pattern.matcher(jsData);
        ArrayList<StockMarketIndexInfo> entities = new ArrayList<>();
        while (matcher.find()) {
            //获取大盘的code
            String marketCode = matcher.group(1);
            //获取其它信息，字符串以逗号间隔
            String otherInfo = matcher.group(2);
            //以逗号切割字符串，形成数组
            String[] splitArr = otherInfo.split(",");
            //大盘名称
            String marketName = splitArr[0];
            //获取当前大盘的开盘点数
            BigDecimal openPoint = new BigDecimal(splitArr[1]);
            //前收盘点
            BigDecimal preClosePoint = new BigDecimal(splitArr[2]);
            BigDecimal curPoint = new BigDecimal(splitArr[3]);
            //获取大盘最高点
            BigDecimal maxPoint = new BigDecimal(splitArr[4]);
            //获取大盘的最低点
            BigDecimal minPoint = new BigDecimal(splitArr[5]);
            //获取成交量
            Long tradeAmt = Long.valueOf(splitArr[8]);
            //获取成交金额
            BigDecimal tradeVol = new BigDecimal(splitArr[9]);
            //时间
            Date curTime = DateTimeUtil.getDateTimeWithoutSecond(splitArr[30] + " " + splitArr[31]).toDate();
            //3：解析的数据封装到实体类中
            StockMarketIndexInfo info = StockMarketIndexInfo.builder()
                    .id(idWorker.nextId())
                    .marketCode(marketCode)
                    .marketName(marketName)
                    .curPoint(curPoint)
                    .openPoint(openPoint)
                    .preClosePoint(preClosePoint)
                    .maxPoint(maxPoint)
                    .minPoint(minPoint)
                    .tradeVolume(tradeVol)
                    .tradeAmount(tradeAmt)
                    .curTime(curTime)
                    .build();
            //收集封装的对象，方便批量插入
            entities.add(info);
        }
        log.info("解析大盘数据完毕！");
        //4：通过mybatis导入数据库
        int count = stockMarketIndexInfoMapper.insertBatch(entities);
        if (count > 0) {
            //大盘采集完毕后，通知backend工程刷新缓存
            //发送日期对象，接收方方便通过接收的日期与当前日期对比，能判断出数据延迟的时长，用于运维通知处理
            rabbitTemplate.convertAndSend("stockExchange","inner.market",new Date());
            log.info("当前时间：{}，插入大盘数据：{}大成功！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), entities);
        } else {
            log.info("当前时间：{}，插入大盘数据：{}失败！！！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), entities);
        }
    }

    @Override
    public void getStockRtIndex() {
        //1：获取所有个股的集合 3000+
        List<String> allCodes = stockBusinessMapper.getAllByStockCodes();
        //2：添加大盘业务前缀
        allCodes = allCodes.stream().map(code -> code.startsWith("6") ? "sh" + code : "sz" + code).collect(Collectors.toList());
        //将所有的个股编码组成的大的集合拆分成若干个小的集合40-->15 15 10
        long startTime = System.currentTimeMillis();
        Lists.partition(allCodes, 15).forEach(codes -> {
            //方案一：分批次串行执行，效率较低，具有较高的延迟
//            //核心思路：将大的集合切割成若干个小的集合，分批次加入Url请求头拉取数据
//            String url = stockInfoConfig.getMarketUrl() + String.join(",", codes);
//            //维护请求头，添加防盗链和用户标识
//            HttpHeaders headers = new HttpHeaders();
//            //防盗链
//            headers.add("Referer", "https://finance.sina.com.cn/stock/");
//            //用户标识
//            headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0");
//            //维护Http请求实体对象
//            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//            //发起请求
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//            int codeValue = responseEntity.getStatusCodeValue();
//            if (codeValue != 200) {
//                //当前请求失败
//                log.error("当前时间点：{}，采集个股数据失败，状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), codeValue);
//                return;
//            }
//            //获取原始js数据
//            String jsData = responseEntity.getBody();
//            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//            log.info("采集个股数据：{}", list);
//            //批量保存采集的个股数据
//            int count = stockRtInfoMapper.insertBatch(list);
//            if (count > 0) {
//                log.info("当前时间：{}，插入个股数据：{}大成功！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
//            } else {
//                log.info("当前时间：{}，插入个股数据：{}失败！！！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
//            }
            //利用多线程，并行执行，提高效率
//            new Thread(()->{
//                //分批次采集
//                String url = stockInfoConfig.getMarketUrl() + String.join(",", codes);
//                //维护请求头，添加防盗链和用户标识
//                HttpHeaders headers = new HttpHeaders();
//                //防盗链
//                headers.add("Referer", "https://finance.sina.com.cn/stock/");
//                //用户标识
//                headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0");
//                //维护Http请求实体对象
//                HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//                //发起请求
//                ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//                int codeValue = responseEntity.getStatusCodeValue();
//                if (codeValue != 200) {
//                    //当前请求失败
//                    log.error("当前时间点：{}，采集个股数据失败，状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), codeValue);
//                    return;
//                }
//                //获取原始js数据
//                String jsData = responseEntity.getBody();
//                List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//                log.info("采集个股数据：{}", list);
//                //批量保存采集的个股数据
//                int count = stockRtInfoMapper.insertBatch(list);
//                if (count > 0) {
//                    log.info("当前时间：{}，插入个股数据：{}大成功！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
//                } else {
//                    log.info("当前时间：{}，插入个股数据：{}失败！！！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
//                }
//            }).start();
            //引入线程池，避免频繁创建线程优于上面的方法
            threadPoolTaskExecutor.execute(()->{
                //分批次采集
                String url = stockInfoConfig.getMarketUrl() + String.join(",", codes);
                //维护请求头，添加防盗链和用户标识
                HttpHeaders headers = new HttpHeaders();
                //防盗链
                headers.add("Referer", "https://finance.sina.com.cn/stock/");
                //用户标识
                headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0");
                //维护Http请求实体对象
                HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
                //发起请求
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
                int codeValue = responseEntity.getStatusCodeValue();
                if (codeValue != 200) {
                    //当前请求失败
                    log.error("当前时间点：{}，采集个股数据失败，状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), codeValue);
                    return;
                }
                //获取原始js数据
                String jsData = responseEntity.getBody();
                List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
                log.info("采集个股数据：{}", list);
                //批量保存采集的个股数据
                int count = stockRtInfoMapper.insertBatch(list);
                if (count > 0) {
                    log.info("当前时间：{}，插入个股数据：{}大成功！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
                } else {
                    log.info("当前时间：{}，插入个股数据：{}失败！！！", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), list);
                }
            });
        });
        log.info("本次采集所花费的时间：{}ms",System.currentTimeMillis()-startTime);
    }

    @PostConstruct
    public void initData() {
        //维护请求头，添加防盗链和用户标识
        HttpHeaders headers = new HttpHeaders();
        //防盗链
        headers.add("Referer", "https://finance.sina.com.cn/stock/");
        //用户标识
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0");
        //维护Http请求实体对象
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
    }
}
