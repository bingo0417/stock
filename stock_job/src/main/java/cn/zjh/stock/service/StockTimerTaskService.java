package cn.zjh.stock.service;

/**
 * @ClassName StockTimerTaskService
 * @Description 股票数据采集接口
 * @Author Rum
 * @Date 2024/7/20 14:55
 * @Version
 **/
public interface StockTimerTaskService {
    /**
     * @Author Rum
     * @Description 获取国内的大盘的实时数据信息
     * @Date 15:05 2024/7/20
     * @Param []
     * @return void
     **/
    void getInnerMarketInfo();

    /**
     * @Author Rum
     * @Description 获取分钟级的股票数据
     * @Date 20:53 2024/7/20
     * @Param
     * @return
     **/
    void getStockRtIndex();
}
