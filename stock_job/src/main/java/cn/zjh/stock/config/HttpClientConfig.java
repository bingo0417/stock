package cn.zjh.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName HttpClientConfig
 * @Description 定义访问http服务的配置类
 * @Author Rum
 * @Date 2024/7/20 11:58
 * @Version
 **/
@Configuration
public class HttpClientConfig {
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
