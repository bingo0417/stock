package cn.zjh.stock.config;

import cn.zjh.stock.pojo.vo.StockInfoConfig;
import cn.zjh.stock.utils.IdWorker;
import cn.zjh.stock.utils.ParserStockInfoUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName CommonConfig
 * @Description //公共配置类
 * @Author Rum
 * @Date 2024/7/12 14:06
 * @Version
 **/
@Configuration
@EnableConfigurationProperties({StockInfoConfig.class })//开启相关对象的加载
public class CommonConfig {
    @Bean
    public IdWorker idWorker(){
        return new IdWorker(1l, 2l);
    }

    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(IdWorker idWorker){
        return new ParserStockInfoUtil(idWorker);
    }
}
