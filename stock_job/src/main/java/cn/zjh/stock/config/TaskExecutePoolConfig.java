package cn.zjh.stock.config;

import cn.zjh.stock.pojo.vo.TaskThreadPoolInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @ClassName TaskExecutePoolConfig
 * @Description 定义线程池的配置类
 * @Author Rum
 * @Date 2024/7/23 9:56
 * @Version
 **/
@Configuration
public class TaskExecutePoolConfig {
    @Autowired
    private TaskThreadPoolInfo info;

    @Bean(name = "threadPoolTaskExecutor",destroyMethod = "shutdown")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //设置核心线程数
        executor.setCorePoolSize(info.getCorePoolSize());
        //设置最大线程数
        executor.setMaxPoolSize(info.getMaxPoolSize());
        //设置空闲最大存活时间
        executor.setKeepAliveSeconds(info.getKeepAliveSeconds());
        //设置任务队列长度
        executor.setQueueCapacity(info.getQueueCapacity());
        //将设置参数初始化
        executor.initialize();
        return executor;
    }
}
