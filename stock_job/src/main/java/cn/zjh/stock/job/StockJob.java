package cn.zjh.stock.job;

import cn.zjh.stock.service.StockTimerTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName StockJob
 * @Description 定义xxl-job执行器的bean的定义
 * @Author Rum
 * @Date 2024/7/22 17:18
 * @Version
 **/
@Component
public class StockJob {

    @Autowired
    private StockTimerTaskService stockTimerTaskService;

    @XxlJob("stockJobHandler")
    public void stockJobHandler(){
        System.out.println("当前时间："+ DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }

    @XxlJob("getInnerMarketInfo")
    public void getInnerMarketInfo(){
        stockTimerTaskService.getInnerMarketInfo();
    }

    @XxlJob("getStockRtIndex")
    public void getStockRtIndex(){
        stockTimerTaskService.getStockRtIndex();
    }
}
