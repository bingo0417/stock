package cn.zjh.stock;

import cn.zjh.stock.service.StockTimerTaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName StockTimerTaskTest
 * @Description 获取大盘数据测试类
 * @Author Rum
 * @Date 2024/7/20 20:07
 * @Version
 **/
@SpringBootTest
public class StockTimerTaskTest {
    @Autowired
    private StockTimerTaskService stockTimerTaskService;

    @Test
    public void test(){
        stockTimerTaskService.getInnerMarketInfo();
    }

    @Test
    public void test1(){
        stockTimerTaskService.getStockRtIndex();
    }
}
