package cn.zjh.stock;

import cn.zjh.stock.mapper.StockBusinessMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName MapperTest
 * @Description StockBusinessMapper的测试类
 * @Author Rum
 * @Date 2024/7/20 20:25
 * @Version
 **/
@SpringBootTest
public class StockBusinessMapperTest {
    @Autowired
    private StockBusinessMapper stockBusinessMapper;

    @Test
    public void test(){
        List<String> allCodes = stockBusinessMapper.getAllByStockCodes();
        System.out.println(allCodes);
        List<String> allCodesList = allCodes.stream().map(code -> code.startsWith("6") ? "sh" + code : "sz" + code).collect(Collectors.toList());
        System.out.println(allCodesList);
    }
}
